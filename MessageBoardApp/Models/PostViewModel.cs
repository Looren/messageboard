﻿using MessageBoardRepository.DomainModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MessageBoardApp.Models
{
    public class PostViewModel
    {
        [Required(ErrorMessage = "Please enter a title")]
        [StringLength(20, ErrorMessage = "Max. 20 characters")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please write a message")]
        public string Content { get; set; }
        public Author Author { get; set; }
        public Category Category { get; set; }
       
        public List<Category> Categories { get; set; }
    }
}
