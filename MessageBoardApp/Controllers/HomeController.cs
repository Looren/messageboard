﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MessageBoardApp.Models;
using MessageBoardRepository.DomainModels;
using MessageBoardRepository.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MessageBoardApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICategoryRepo CatRepos;
        private readonly IAuthorRepo AuthorRepos;
        private readonly IPostRepo PostRepos;

        public HomeController(ICategoryRepo catRepos, IAuthorRepo authorRepos, IPostRepo postRepos)
        {
            CatRepos = catRepos;
            AuthorRepos = authorRepos;
            PostRepos = postRepos;
        }

        public IActionResult Index()
        {
            return View(PostRepos.GetPosts());
        }

        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Post post = PostRepos.GetPostById(id.Value);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        public IActionResult Create()
        {
            PostViewModel vm = new PostViewModel();
            vm.Categories = CatRepos.GetCategories();
            return View(vm);
        }

        // POST: Home/Create
        [HttpPost]
        public IActionResult Create(PostViewModel viewModel)
        {
            viewModel.Categories = CatRepos.GetCategories();
            if (ModelState.IsValid)
            {
                Post post = new Post();
                post.Category = viewModel.Category;
                post.Title = viewModel.Title;
                post.Content = viewModel.Content;
                post.Author = viewModel.Author;

                PostRepos.InsertPost(post);
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }



        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Post post = PostRepos.GetPostById(id.Value);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, Post post)
        {
            if (id != post.PostId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    repository.Update(person);
                }
                catch (DbUpdateConcurrencyException)
                {
                    var per = repository.GetItemById(person.Id);
                    if (per == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(person);
        }
    }

    //// GET: Home/Delete/5
    //public IActionResult Delete(int? id)
    //{
    //    if (id == null)
    //    {
    //        return NotFound();
    //    }

    //    var person = repository.GetItemById(id.Value);
    //    if (person == null)
    //    {
    //        return NotFound();
    //    }

    //    return View(person);
    //}

    //// POST: Home/Delete/5
    //[HttpPost, ActionName("Delete")]
    //[ValidateAntiForgeryToken]
    //public IActionResult DeleteConfirmed(int id)
    //{
    //    var person = repository.GetItemById(id);
    //    repository.Remove(id);
    //    return RedirectToAction("Index");
    //}

}