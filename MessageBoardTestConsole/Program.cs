﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace MessageBoardTestConsole
{
    public class Program
    {
        static string connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=MessageBoardDb;" +
            "Integrated Security=True";

        static void Main(string[] args)
        {
            InsertNewPost("title", "Content", "Nicolai2", "Tøj2");
            Console.ReadLine();
        }

        static int InsertNewCategory(string catName)
        {
            SqlConnection con = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand("spInsertCategory", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Name", catName);

            SqlParameter param = new SqlParameter("@CatID", SqlDbType.Int);
            param.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            int newCatID = Convert.ToInt32(cmd.Parameters["@CatID"].Value);
            return newCatID;
        }

        static int InsertNewAuthor(string authorName)
        {
            SqlConnection con = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand("spInsertAuthor", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Name", authorName);

            SqlParameter param = new SqlParameter("@AuthorID", SqlDbType.Int);
            param.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            int newAuthorID = Convert.ToInt32(cmd.Parameters["@AuthorID"].Value);
            return newAuthorID;
        }

        static void InsertNewPost(string postTitle, string postContent, string authorName, string catName)
        {
            SqlConnection con = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand("spInsertpost", con);
            cmd.CommandType = CommandType.StoredProcedure;

            int? authorID = GetAuthorID(authorName);
            int? catID = getCategoryID(catName);

            if (authorID != null)
            {
                cmd.Parameters.AddWithValue("@AuthorID", authorID);
            }
            else
            {
                authorID = InsertNewAuthor(authorName);
                cmd.Parameters.AddWithValue("@AuthorID", authorID);
            }

            if (catID != null)
            {
                cmd.Parameters.AddWithValue("@CategoryID", catID);
            }
            else
            {
                catID = InsertNewCategory(catName);
                cmd.Parameters.AddWithValue("@CategoryID", catID);
            }

            cmd.Parameters.AddWithValue("@Title", postTitle);
            cmd.Parameters.AddWithValue("@content", postContent);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

        }

        private static int? getCategoryID(string catName)
        {
            SqlConnection con = new SqlConnection(connectionString);

            string sqlString = "SELECT CategoryID FROM Category";
            sqlString += " WHERE Name = @Name";

            SqlCommand cmd = new SqlCommand(sqlString, con);

            cmd.Parameters.AddWithValue("@Name", catName);

            int? catID = null;

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if (reader.HasRows)
            {
                catID = Convert.ToInt32(reader["CategoryId"]);
            }

            con.Close();

            return catID;
        }

        static int? GetAuthorID(string authorName)
        {
            SqlConnection con = new SqlConnection(connectionString);

            string sqlString = "SELECT AuthorID FROM Author";
            sqlString += " WHERE Name = @Name";

            SqlCommand cmd = new SqlCommand(sqlString, con);

            cmd.Parameters.AddWithValue("@Name", authorName);

            int? authorID = null;

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if (reader.HasRows)
            {
                authorID = Convert.ToInt32(reader["AuthorId"]);
            }

            con.Close();


            return authorID;
        }
    }
}
