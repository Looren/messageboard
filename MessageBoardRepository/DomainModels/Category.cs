﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MessageBoardRepository.DomainModels
{
    public class Category
    { 
        public int CategoryId { get; set; }
        public string Name { get; set; }

    }
}
