﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MessageBoardRepository.DomainModels
{
    public class Author
    {
        public int AuthorId { get; set; }

        [Required(ErrorMessage = "Please enter your name")]
        [StringLength(20, ErrorMessage = "Max. 20 characters")]
        public string Name { get; set; }
    }
}
