﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using MessageBoardRepository.DomainModels;

namespace MessageBoardRepository.Repositories
{
    public class AuthorRepo : IAuthorRepo
    {
        private readonly string _conString;

        public AuthorRepo(string conString)
        {
            _conString = conString;
        }

        public int GetAuthorID(string authorName)
        {
            SqlConnection con = new SqlConnection(_conString);

            string sqlString = "SELECT AuthorID FROM Author";
            sqlString += " WHERE AuthorName = @Name";

            SqlCommand cmd = new SqlCommand(sqlString, con);

            cmd.Parameters.AddWithValue("@Name", authorName);

            int authorID = 0;

            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows)
                {
                    authorID = Convert.ToInt32(reader["AuthorId"]);
                }
            }
            catch (SqlException e)
            {
                string sqlError = e.Message;
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

                con.Close();
            }



            return authorID;
        }

        public int InsertAuthor(Author author)
        {
            SqlConnection con = new SqlConnection(_conString);

            SqlCommand cmd = new SqlCommand("spInsertAuthor", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Name", author.Name);

            SqlParameter param = new SqlParameter("@AuthorID", SqlDbType.Int);
            param.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            int newAuthorID = Convert.ToInt32(cmd.Parameters["@AuthorID"].Value);
            return newAuthorID;
        }
    }
}
