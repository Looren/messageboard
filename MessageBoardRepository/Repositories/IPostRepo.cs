﻿using MessageBoardRepository.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageBoardRepository.Repositories
{
    public interface IPostRepo
    {
        void InsertPost(Post post);
        List<Post> GetPosts();
        Post GetPostById(int postId);
    }
}
