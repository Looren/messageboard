﻿using MessageBoardRepository.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageBoardRepository.Repositories
{
    public interface IAuthorRepo
    {
        int InsertAuthor(Author author);
        int GetAuthorID(string authorName);
    }
}
