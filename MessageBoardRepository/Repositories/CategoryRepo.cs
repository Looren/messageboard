﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using MessageBoardRepository.DomainModels;

namespace MessageBoardRepository.Repositories
{
    public class CategoryRepo : ICategoryRepo
    {
        private readonly string _conString;

        public CategoryRepo(string conString)
        {
            _conString = conString;
        }

        public List<Category> GetCategories()
        {
            SqlConnection con = new SqlConnection(_conString);

            string sqlString = "SELECT * FROM Category";

            SqlCommand cmd = new SqlCommand(sqlString, con);
            SqlDataReader reader = null;

            List<Category> categories = new List<Category>();

            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        categories.Add(new Category
                        {
                            CategoryId = Convert.ToInt32(reader["CategoryId"]),
                            Name = reader["CategoryName"].ToString()
                        });
                    }
                }

            }
            catch (SqlException e)
            {
                throw;
            }
            catch (Exception e)
            {

                throw;
            }
            finally
            {
                con.Close();
            }

            return categories;
        }

        public int InsertCategory(Category category)
        {
            SqlConnection con = new SqlConnection(_conString);

            SqlCommand cmd = new SqlCommand("spInsertCategory", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Name", category.Name);

            SqlParameter param = new SqlParameter("@CatID", SqlDbType.Int);
            param.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            int newCatID = Convert.ToInt32(cmd.Parameters["@CatID"].Value);
            return newCatID;
        }
    }
}
