﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using MessageBoardRepository.DomainModels;
using Microsoft.AspNetCore.Rewrite.Internal.UrlActions;

namespace MessageBoardRepository.Repositories
{
    public class PostRepo : IPostRepo
    {
        private readonly string _conString;

        public PostRepo(string conString)
        {
            _conString = conString;
        }

        public Post GetPostById(int postId)
        {
            SqlConnection con = new SqlConnection(_conString);

            SqlCommand cmd = new SqlCommand("spGetPost", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = null;

            cmd.Parameters.AddWithValue("@PostID", postId);

            Post post = new Post();

            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        post.PostId = Convert.ToInt32(reader["PostId"]);
                        post.Title = reader["Title"].ToString();
                        post.Content = reader["Content"].ToString();
                        post.Date = Convert.ToDateTime(reader["Date"]);
                        post.Category = new Category
                        {
                            CategoryId = Convert.ToInt32(reader["CategoryId"]),
                            Name = reader["CategoryName"].ToString()
                        };
                        post.Author = new Author
                        {
                            AuthorId = Convert.ToInt32(reader["AuthorId"]),
                            Name = reader["AuthorName"].ToString()
                        };
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }

            return post;
        }

        public List<Post> GetPosts()
        {
            SqlConnection con = new SqlConnection(_conString);

            SqlCommand cmd = new SqlCommand("spGetPosts", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = null;

            List<Post> posts = new List<Post>();

            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        posts.Add(new Post
                        {
                            PostId = Convert.ToInt32(reader["PostId"]),
                            Title = reader["Title"].ToString(),
                            Content = reader["Content"].ToString(),
                            Date = Convert.ToDateTime(reader["Date"]),
                            Category = new Category
                            {
                                CategoryId = Convert.ToInt32(reader["CategoryId"]),
                                Name = reader["CategoryName"].ToString()
                            },
                            Author = new Author
                            {
                                AuthorId = Convert.ToInt32(reader["AuthorId"]),
                                Name = reader["AuthorName"].ToString()
                            }
                        });
                    }

                }
            }
            catch (SqlException e)
            {
                throw;
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                con.Close();
            }

            return posts;
        }

        public void InsertPost(Post post)
        {
            SqlConnection con = new SqlConnection(_conString);

            SqlCommand cmd = new SqlCommand("spInsertpost", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@AuthorID", post.Author.AuthorId);
            cmd.Parameters.AddWithValue("@AuthorName", post.Author.Name);
            cmd.Parameters.AddWithValue("@CategoryID", post.Category.CategoryId);
            cmd.Parameters.AddWithValue("@Title", post.Title);
            cmd.Parameters.AddWithValue("@content", post.Content);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}
