﻿using MessageBoardRepository.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageBoardRepository.Repositories
{
    public interface ICategoryRepo
    {
        int InsertCategory(Category category);
        List<Category> GetCategories();
    }
}
