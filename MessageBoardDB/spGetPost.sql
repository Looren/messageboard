﻿CREATE PROCEDURE [dbo].[spGetPost]
	@PostID int
AS
	SELECT * FROM Post
	LEFT JOIN Author ON post.AuthorId = Author.AuthorId
	LEFT JOIN Category ON post.CategoryId = Category.CategoryId
	WHERE post.PostId = @PostID