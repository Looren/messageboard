﻿CREATE PROCEDURE [dbo].[spInsertCategory]
	@Name nvarchar(50),
	@CatID int OUTPUT
AS

	INSERT INTO Category(CategoryName)
	VALUES(@Name)
	SELECT @CatID = SCOPE_IDENTITY()