﻿CREATE TABLE [dbo].[Post]
(
	[PostId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Title] NVARCHAR(50) NOT NULL, 
    [Content] NVARCHAR(MAX) NULL, 
    [Date] DATE NULL, 
    [AuthorId] INT NOT NULL, 
    [CategoryId] INT NOT NULL, 
    CONSTRAINT [FK_Post_Author] FOREIGN KEY (AuthorId) REFERENCES Author(AuthorId), 
    CONSTRAINT [FK_Post_Category] FOREIGN KEY (CategoryId) REFERENCES Category(CategoryId)
)
