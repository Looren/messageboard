﻿CREATE PROCEDURE [dbo].[spInsertAuthor]
	@Name nvarchar(50),
	@AuthorID int OUTPUT
AS

	INSERT INTO Author(AuthorName)
	VALUES(@Name)
	SELECT @AuthorID = SCOPE_IDENTITY()