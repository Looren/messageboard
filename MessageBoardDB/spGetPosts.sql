﻿CREATE PROCEDURE [dbo].[spGetPosts]
AS
	SELECT * FROM Post 
	LEFT JOIN Author ON Post.AuthorId = Author.AuthorId
	LEFT JOIN Category ON Post.CategoryId = Category.CategoryId
	ORDER BY [Date] DESC
