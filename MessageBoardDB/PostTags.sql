﻿CREATE TABLE [dbo].[PostTags]
(
	[TagId] INT NOT NULL , 
    [PostId] INT NOT NULL, 
    PRIMARY KEY ([PostId], [TagId]), 
    CONSTRAINT [FK_PostTags_Tag] FOREIGN KEY (TagId) REFERENCES Tag(TagId), 
    CONSTRAINT [FK_PostTags_Post] FOREIGN KEY (PostId) REFERENCES post(PostId)
)
