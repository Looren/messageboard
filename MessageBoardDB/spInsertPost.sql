﻿CREATE PROCEDURE [dbo].[spInsertPost]
	@AuthorId int,
	@AuthorName nvarchar(50),
	@CategoryId int,
	@Title nvarchar(50),
	@content nvarchar(MAX)
AS

DECLARE @ExistingAuthorID int = 0

SELECT @ExistingAuthorID = AuthorId From Author WHERE AuthorName = @AuthorName
IF @ExistingAuthorID = 0
BEGIN
	DECLARE @NewAuthorId int
	EXEC spInsertAuthor @AuthorName, @NewAuthorID OUTPUT
	INSERT INTO Post(Title, Content, Date, AuthorId, CategoryId)
	VALUES(@Title,@content,GETDATE(),@NewAuthorId,@CategoryId);
END
ELSE
BEGIN
	INSERT INTO Post(Title, Content, Date, AuthorId, CategoryId)
	VALUES(@Title,@content,GETDATE(),@ExistingAuthorID,@CategoryID)
END